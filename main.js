/**
 * Created by Dmitry on 22.09.2014.
 */
/**
 * Bezier
 * MIT Licensed
 * @author Mamod Mehyar
 * http://twitter.com/mamod
 * version 1.1
 * 23-9-2013
 */

(function () {
    function h(b) {
        var c;
        this.X = 0;
        this.Y = 1;
        this.CX = 2;
        this.CY = 3;
        this.CURV = [];
        this.line = [];
        this.ctrl = [];
        for (var a = b.length / 2, d = 0; d < a;)this.ctrl.push(b.splice(0, 2)), d++;
        b = this.ctrl.length - 1;
        for (a = 0; a <= b; a++)c = 0 === a ? 1 : 1 === a ? b : c * ((b - a + 1) / a), this.ctrl[a][this.CX] = this.ctrl[a][this.X] * c, this.ctrl[a][this.CY] = this.ctrl[a][this.Y] * c
    }

    h.prototype = {bezier: function () {
        var b = [].splice.call(arguments, 0);
        this.ctrl = (new h(b)).ctrl;
        return this
    }, point: function (b) {
        var c = this.ctrl, a = [], d = c.length - 1, f = b;
        a.push([c[0][this.CX],
            c[0][this.CY]]);
        for (var g = 1; g <= d; g++)a.push([c[g][this.CX] * f, c[g][this.CY] * f]), f *= b;
        c = a[d];
        f = b = 1 - b;
        for (g = d - 1; 0 <= g; g--)c[this.X] += a[g][this.X] * f, c[this.Y] += a[g][this.Y] * f, f *= b;
        return c
    }, curve: function (b) {
        var c = [];
        b--;
        for (var a = 0; a <= b; a++)c.push(this.point(a / b));
        this.CURV = c;
        return this
    }, spline: function (b, c, a, d) {
        a -= b;
        d -= c;
        for (var f = this.CURV.length, g = this.line.length, h = 0, m, n = 0, q = this.line, p = this.CURV, l = 0; l < f; l++) {
            var k = p[l][0], e = p[l][1];
            n++;
            m = l + g;
            var r = q, s = (d ? k * a + b : e * a + b).toFixed(2), t = (e * d + c).toFixed(2),
                k = d ? k - e : k, e = k - h, e = d ? e * (n / 100) : e, h = k, e = 0 > e ? -e : e;
            r[m] = {x: s, y: t, easing: e}
        }
        return this
    }, getSpline: function () {
        return this.line
    }, getCurve: function () {
        return this.CURV
    }};
    window.bezier = function (b) {
        "object" !== typeof b && (b = [].splice.call(arguments, 0));
        return new h(b)
    }
})();

var casper = require('casper').create({
//    clientScripts: [
//        './includes/bower_components/jquery/dist/jquery.min.js',      // These two scripts will be injected in remote
//        './includes/bower_components/underscore/underscore-min.js'   // DOM on every request
//    ],
    pageSettings: {
        loadImages: true,        // The WebPage instance used by Casper will
        loadPlugins: true         // use these settings
    },
    logLevel: "info",              // Only "info" level messages will be logged
    verbose: true,                  // log messages will be printed out to the console
    viewportSize: {
        width: 1920,
        height: 1280
    }
});
var mouse = require("mouse").create(casper);
var x = require('casper').selectXPath;

var url = "";
var deep = 10;

function getPosition() {
    return [Math.floor(1920 * Math.random()), Math.floor(1280 * Math.random())]
}
function createMoveMap(x0, y0, x1, y1, x2, y2, x3, y3, x4, y4, points) {
    var b = bezier(x0, y0, x1, y1, x2, y2, x3, y3, x4, y4);
    console.log(b);
    b.curve(points);
    return b.getCurve();
}
function RandomMove() {
    var point0 = getPosition();
    var point1 = getPosition();
    var point2 = getPosition();
    var point3 = getPosition();
    var point4 = getPosition();
    return createMoveMap(
        point0[0],
        point0[1],
        point1[0],
        point1[1],
        point2[0],
        point2[1],
        point3[0],
        point3[1],
        point4[0],
        point4[1], 30);
}

casper.moveCursorOver = function (c, i) {
    return this.then(function () {
        var point = {'x': c[i][0], 'y': c[i][1]};
        var time = Math.floor(Math.random() * 50);
        time = time + 50;
        this.echo(i);
        if (i >= 29) {
            this.navigator();
        }
        this.wait(time, function () {
            this.echo("Point X: " + point.x + "Y: " + point.y);
            this.mouse.move(point.x, point.y);
            i++;
            this.moveCursorOver(c, i);
        });

    });
};
casper.getRandomUrlHref = function () {
    return this.evaluate(function () {
        var links = __utils__.findAll('a[href]');
        return links[Math.floor(Math.random() * (links.length))].pathname;
    });
};
casper.navigator = function () {
    return this.then(function () {
        if (Math.random() > 0.5) {
            this.echo(this.getCurrentUrl());
            url = this.getRandomUrlHref();
            this.echo("URL:   " + url);
            this.moveWhileSelector('a[href*="' + url + '"]');
            this.next();

        } else {
            var c = RandomMove();
            this.moveCursorOver(c, 0);
        }

    });
};
casper.moveWhileSelector = function (selector) {
    return this.then(function () {
            if (this.exists(selector)) {
                if (Math.random() >= 0.5) {
                    var position = getPosition();
                    this.echo("True: X =" + position[0] + "; Y =" + position[1]);
                    this.mouse.move(position[0], position[1]);
                    var time = Math.floor(Math.random() * 2000);
                    time = time + 50;
                    this.echo("Wait: " + time);
                    this.wait(time, function () {
                        this.moveWhileSelector(selector);
                    });
                } else {
                    if (Math.random() <= 0.5) {
                        var time = Math.floor(Math.random() * 10000);
                        time = time + 50;
                        this.echo('Found link: ' + this.getElementInfo(selector).tag);
                        this.echo("Click: " + this.getElementInfo(selector).tag);
                        this.echo("Wait: " + time);
                        this.waitUntilVisible(selector, function () {
                            this.wait(time, function () {
                                this.mouse.click(selector);
                                if (selector.indexOf("/") >= 0) {
                                    this.echo(this.getElementAttribute(selector, 'href'));
                                    this.thenOpen(this.getElementAttribute(selector, 'href'));
                                }//is linkable
                            });
                        });

                    }
                    var position = getPosition();
                    this.scrollTo(position[0], position[1]);
                    this.echo("Scroll to");
                    var time = Math.floor(Math.random() * 5000);
                    time = time + 50;
                    this.echo("Wait: " + time);
                    this.wait(time, function () {
                        this.navigator();
                    });
                }
            } else {
                this.navigator();
            }
        }
    )
};

casper.start('http://chronogold.tictail.com', function () {//Учет в GoogleAnalytics
    this.page.customHeaders = {
        "Referer" : "http://google.com/"
    };
});

casper.next = function () {
    this.navigator();
};
var myArray = ["Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.36 Safari/525.19",
    ",Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.481.0 Safari/534.4",
    ",Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.86 Safari/533.4",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, like Gecko) Chrome/4.0.223.3 Safari/532.2",
    ",Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/4.0.201.1 Safari/532.0",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.5 (KHTML, like Gecko) Chrome/2.0.173.1 Safari/530.5",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.558.0 Safari/534.10",
    ",Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Gecko) Chrome/9.1.0.0 Safari/540.0",
    ",Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.600.0 Safari/534.14",
    ",Mozilla/5.0 (X11; U; Windows NT 6; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.587.0 Safari/534.12",
    ",Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
    ",Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16",
    ",Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
    ",Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.792.0 Safari/535.1",
    ",Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.872.0 Safari/535.2",
    ",Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7",
    ",Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
    ",Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.45 Safari/535.19",
    ",Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
    ",Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
    ",Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
    ",Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.15 (KHTML, like Gecko) Chrome/24.0.1295.0 Safari/537.15",
    ",Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36",
    ",Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1467.0 Safari/537.36",
    ",Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36",
    ",Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1623.0 Safari/537.36",
    ",Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36",
    ",Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36"]
casper.userAgent(myArray[Math.floor(Math.random() * myArray.length)]);
casper.navigator().run();
